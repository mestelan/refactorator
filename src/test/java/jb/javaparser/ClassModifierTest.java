package jb.javaparser;

import jb.util.FileUtils;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;


public final class ClassModifierTest {


    static final String ROOT_59 = "/home/jbm/workspaces/intellij/v1103-sp__5.9.x/";
    static final String ROOT_TRUNK_TEST = "/home/jbm/workspaces/intellij/trunk/_ebx-test/src/";

    @Test
    public void testAddStaticImport() throws IOException {
        boolean isTest = false;
        ClassModifier x = new ClassModifier();
        if (isTest) {
            Path path = Paths.get("/home/jbm/workspaces/intellij/v1103-sp__5.9.x/_ebx/src/com/orchestranetworks/cce/rdb/rqe/RqeProjectionFunction.java");
            x.searchFields(path);
        } else {
            final List<String> paths = Arrays.asList(
                    "_ebx-test",
                    "_ebx");
            for (String folders : paths) {
                x.searchFields(Paths.get(ROOT_59, folders, "src"));
            }
        }
    }

    @Test
    public void testRemoveListOfAnnotationsByDirectory() throws IOException {
        ClassModifier x = new ClassModifier();
        x.searchMarkedMethods(Paths.get(ROOT_TRUNK_TEST));
    }

    @Test
    public void testRemoveAnnotationsByMethod() throws IOException {

        String spec = "/home/jbm/tmp/skipped";
        final List<String> lines = FileUtils.readAllLines(spec);
        for (String line : lines) {
            final int i = line.lastIndexOf(".");
            final String method = line.substring(i + 1);
            final String fqn = line.substring(0, i);
            final Path path = FileUtils.qualifiedNameToPath(ROOT_TRUNK_TEST, fqn);
            ClassModifier x = new ClassModifier();
            x.modifyClassByRemovingAnnotation(path, method);
        }
    }

}