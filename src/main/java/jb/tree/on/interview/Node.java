package jb.tree.on.interview;

import java.util.List;

public abstract class Node {
    /**
     * Returns the children of this node.
     */
    public abstract List<Node> getChildren();

    /**
     * Returns the value of this node.
     */
    public abstract int getValue();

    /**
     * Returns the sum of values of all descendants
     * (children, children of children, etc., recursively).
     */
    public int getSumOfAllDescendants() {
        // To be implemented.
        return -1;
    }

    /**
     * Returns the List of all descendants (children, children of
     * children, etc., recursively).
     */
    public List<Node> getAllDescendants() {
        // To be implemented.
        return null;
    }
}
