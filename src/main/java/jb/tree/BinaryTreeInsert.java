/*
 * Copyright TIBCO Software Inc. 2000-2021. All rights reserved.
 */

package jb.tree;

import java.util.*;

class BinaryTreeInsert<U extends Comparable>
{
	public static void main(String[] args)
	{
		List<Integer> values = Arrays.asList(1, 5, 9, 8, 4, 64);
		final BinaryTreeInsert<Integer> x = new BinaryTreeInsert<>();
		final Node root = new Node(values.get(0));
		for (int i = 1; i < values.size(); i++)
			root.insert(values.get(i));

		x.visitInOrder(root);
	}


	void visitInOrder(Node<U> root)
	{
		boolean visitedLeft = false;

		Node<U> current = root;
		while (current != null)
		{
			if (!visitedLeft)
			{
				while (current.left != null)
				{
					current = current.left;
				}
			}

			visit(current);

			visitedLeft = true;

			if (current.right != null)
			{
				visitedLeft = false;
				current = current.right;
			}

			else if (current.parent != null)
			{
				while (current.parent != null
					&& current == current.parent.right)
					current = current.parent;

				if (current.parent == null)
					break;
				current = current.parent;
			}
			else
				break;
		}
	}

	private static void visit(Node current)
	{
		System.out.print(current.getValue() + ", ");
	}

	static final class Node<T extends Comparable>
	{
		T value;
		Node<T> left;
		Node<T> right;
		Node<T> parent;

		public Node(T aValue)
		{
			this.value = aValue;
		}

		T getValue()
		{
			return this.value;
		}
		
		void insert(T value)
		{
			if (this.getValue().compareTo(value) > 0)
			{
				if (this.left == null)
				{
					this.left = new Node<>(value);
					this.left.parent = this;
				}
				else
					this.left.insert(value);
			}

			else
			{
				if (this.right == null) {
					this.right = new Node<>(value);
					this.right.parent = this;
				} else
					this.right.insert(value);
			}
		}
	}
}