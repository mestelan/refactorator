package jb.jdbi;

import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.statement.Update;

public final class TablePopulator {

    void populate() {
        Jdbi jdbi = Jdbi.create("jdbc:postgresql://localhost:5432/ebx", "ebx", "ebx");

        jdbi.useHandle(handle -> {
            for (int tx = 4_001; tx < 4_999; tx++) {
                doTransaction(handle, tx);
            }
        });
    }

    private void doTransaction(org.jdbi.v3.core.Handle handle, int tx) {
        handle.begin();
        for (int pk = 0; pk < 9_999; pk++) {
            insertTxRow(tx, handle);
            insertHistoryRow(tx, pk, handle);
            insertTxStatRow(tx, handle);
        }
        handle.commit();
    }

    private void insertHistoryRow(int tx, int pk, Handle handle) {
        handle.createUpdate("insert into ebx_hg_table (tx_id, instance_id, op, pk, opk, f1, of1 ) VALUES" +
                "(:tx_id, 1, 'U', :pk,'',:field, 'M')")
                .bind("tx_id", tx)
                .bind("pk", pk)
                .bind("field", -1).execute();
    }

    private void insertTxRow(int tx, Handle handle) {
        handle.createUpdate("insert into ebx_hv_tx " +
                "(tx_id, user_id, action_id, home_id, session_id, timestamp, uuid, execution_info)" +
                " VALUES (:tx_id, 1, 10, 1, 2001, 1621430550207, '433ABE50-B8A5-11EB-AEAE-00B82885897C', '')")
                .bind("tx_id", tx).execute();
    }

    private void insertTxStatRow(int tx, Handle handle) {
        handle.createUpdate("insert into ebx_hv_tx_stat " +
                "(tx_id, instance_id, txs, nb_creates, nb_updates, nb_deletes) " +
                "VALUES (:tx_id, 1, 1, 0, 9999, 0)")
                .bind("tx_id", tx).execute();
    }

    public static void main(String[] args) {
        final TablePopulator x = new TablePopulator();
        x.populate();
    }
}
