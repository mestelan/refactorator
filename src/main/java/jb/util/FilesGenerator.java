package jb.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

public final class FilesGenerator {

    private static final String IN_FILENAME = "/home/jbm/workspaces/intellij/trunk_dub/_ebx-test/src/com/onwbp/adaptation/perspective/hierarchy/test/log";
    private static final String OUT_DIR = "/home/jbm/workspaces/intellij/trunk_dub/_ebx-test/src-suite/com/onwbp/adaptation/test/values/synthetic";
    public static final String HLJTS_JSC_CB_TEST_CLASS = "HLJtsJscCbTest.class";

    public static final String header = String.join("\n",
            "package com.onwbp.adaptation.test.values.synthetic;",
            "import org.junit.runner.*;",
            "import com.onwbp.adaptation.perspective.hierarchy.test.*;",
            "import org.junit.runners.*;",
            "import org.junit.runners.Suite.*;",
            "@RunWith(Suite.class)",
            "@SuiteClasses({");

    String accu = this.header;
    boolean first = true;

    public static final String footer = "{}";

    private void generate() throws IOException {
        final List<String> hierarchyTests = Files.readAllLines(Paths.get(IN_FILENAME));
        for (String test : hierarchyTests) {
            this.feedTemplate(test);
        }
    }

    private String feedTemplate(String test) {

        final String testClass = test + ".class";
        if (first) {
            accu += testClass;
            first = false;
        } else {
            accu += ",\n" + testClass;
        }

        final String className = "My" + test;
        final Path outFile = Paths.get(OUT_DIR, className +
                ".java");
        final String content = accu + ",\n" + HLJTS_JSC_CB_TEST_CLASS + "})" + "\npublic final class "
                + className
                + this.footer;
        this.writeOut(outFile, content);

        return content;
    }

    private void writeOut(Path outFile, String lines) {
        try {
            Files.write(outFile, Collections.singletonList(lines));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) throws IOException {
        final FilesGenerator x = new FilesGenerator();
        x.generate();
    }
}
