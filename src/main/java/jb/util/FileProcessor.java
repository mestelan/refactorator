package jb.util;

import java.io.*;
import java.nio.file.*;
import java.util.*;

/**
 * Applies a simple transformation from in_file to out_file
 */

public final class FileProcessor
{
	private static final String IN_FILENAME = "/home/jbm/Downloads/canada/EBX_BLK.csv";
	private static final String OUT_FILENAME = "/home/jbm/Downloads/canada/EBX_BLK_trimmed.csv";

	private void parseFile() throws IOException
	{
		final List<String> linesIn = Files.readAllLines(Paths.get(IN_FILENAME));
		List<String> linesOut = new ArrayList<>(linesIn.size());

		for (String line : linesIn)
		{
			final String out = processLine(line);
			linesOut.add(out);
		}
		this.writeOut(linesOut);
	}

	private String processLine(String line)
	{
		String out = "";
		final char[] chars = line.toCharArray();
		for (char c : chars)
		{
			if (c != 8239)
				out += c;
		}
		return out;
	}

	private void writeOut(List<String> linesOut) throws IOException
	{
		System.out.println(linesOut);
		Files.write(Paths.get(OUT_FILENAME), linesOut);
	}

	public static void main(String[] args) throws IOException
	{
		final FileProcessor x = new FileProcessor();
		x.parseFile();
	}
}
