package jb.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Writes variations along a template.
 */
public final class StringTemplateLoopPrinter {
    private static final String IN_FILENAME = "/mnt/vmtmpfs/ebx.test.cp-x/2021-06-17.10-53-42.222/log/__testReports__/sorted";
    private static final String OUT_FILENAME = "/mnt/vmtmpfs/ebx.test.cp-x/2021-06-17.10-53-42.222/log/__testReports__/sorted-added.txt";

    private static final String PAYLOAD = "com.orchestranetworks.service.archive.TestArchiveCustomExport";

    public static final String TEMPLATE = """
            <Context path="/%s" docBase="_ebx-eclipse/catalina.base/conf/webapps/%s" />""";
    private int counter;

    private void loop() throws IOException {
        final List<String> linesIn = Files.readAllLines(Paths.get(IN_FILENAME));
        Collections.shuffle(linesIn);
        final List<String> linesOut = linesIn.stream().map(this::transform).collect(Collectors.toList());
        writeOut(linesOut);
    }

    private String transform(String lineIn) {
        final int freq = 2;
        if (this.counter++ % freq == 0)
            return lineIn + "\n" + PAYLOAD;
        return lineIn;
    }

    private void writeOut(Iterable<String> linesOut) throws IOException {
        System.out.println(linesOut);
        Files.write(Paths.get(OUT_FILENAME), linesOut);
    }

    public static void main(String[] args) throws IOException {
        final StringTemplateLoopPrinter x = new StringTemplateLoopPrinter();
        x.loop();
    }
}
