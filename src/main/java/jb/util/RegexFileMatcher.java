package jb.util;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.regex.*;

public final class RegexFileMatcher
{
	private static final String IN_FILE_NAME = "/mnt/vmtmpfs/ebx.test.trunk/2021-04-29.17-18-48.950/log/__testReports__/error_summary.csv";

	void captureConstantName(String filename) throws IOException
	{
		Set<String> names = new HashSet<>();

		Pattern pattern = Pattern.compile(
			".*;"
				+ "([^\\]]*)"
				+ " should have failed. Delete .*");
		for (String line : Files.readAllLines(Paths.get(filename)))
		{
			final Matcher matcher = pattern.matcher(line);
			if (matcher.matches())
			{
				final String name = matcher.group(1);
				names.add(name);
			}
		}
		names.forEach(System.out::println);
		System.out.println(names.size());
	}

	void captureDecimal(String filename) throws IOException
	{
		Pattern pattern = Pattern.compile(".* GET REST operation .* in (\\d+) .*");
		long sum = 0;
		int nb = 0;
		for (String line : Files.readAllLines(Paths.get(filename)))
		{
			final Matcher matcher = pattern.matcher(line);
			if (matcher.matches())
			{
				final String duration = matcher.group(1);
				sum += Integer.parseInt(duration);
				nb++;
			}
		}

		System.out.println(nb + "requests in " + sum + " ms");
		System.out.println("Average=" + (sum / nb) + " ms");
	}

	public static void main(String[] args) throws IOException
	{
		final RegexFileMatcher x = new RegexFileMatcher();
		x.captureConstantName(IN_FILE_NAME);
	}
}
