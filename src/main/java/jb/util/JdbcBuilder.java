package jb.util;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class JdbcBuilder {
    List<String> COLUMNS = Arrays.asList(
            "tx_id",
            "instance_id",
            "txs",
            "nb_creates",
            "nb_updates",
            "nb_deletes");

    String insert = "insert into ebx_hv_tx_stat (%s) VALUES (%s)";

    String createStatement() {
        final String cols = String.join(", ", COLUMNS);
        final String vals = String.join(", ",
                COLUMNS.stream().map(col -> ":" + col).collect(Collectors.toList()));
        final String statement = String.format(insert, cols, vals);
        return statement;
    }

    public static void main(String[] args) {
        final JdbcBuilder x = new JdbcBuilder();
        System.out.println(x.createStatement());
    }


}
