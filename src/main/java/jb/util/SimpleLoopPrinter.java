package jb.util;

/**
 * Writes variations along a template.
 */
final class SimpleLoopPrinter {

    private static final String OUT_FILENAME = "/mnt/vmtmpfs/ebx.test.cp-x/2021-06-17.10-53-42.222/log/__testReports__/sorted-added.txt";

    public static final String TEMPLATE = """
            	@Test
            	@IgnoreLimitation(cause = { BugReference.INCOMPATIBLE_WITH_HISTORY_MODE })
            	public void testDuplicateTransitiveLink¤() throws Exception
            	{
            		UpgGeneratorHelper.VARIABLE_SEED = ¤;
            		final String ini = "tableRefPath-transitive-dup-noHistory.xsd";
            		final String mod = "tableRefPath-ini-noHistory.xsd";
            		this.modifySchemaAndRefresh(ini, ini);
            		this.modifySchemaAndRefresh(ini, mod);
            	}
            """;

    private void loop() {
        for (int i = 0; i < 100; i++) {
            String s = TEMPLATE.replaceAll("¤", i + "");
            System.out.println(s);
        }

    }


    public static void main(String[] args) {

        final SimpleLoopPrinter x = new SimpleLoopPrinter();
        x.loop();
    }

}
