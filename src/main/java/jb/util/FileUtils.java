package jb.util;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class FileUtils {

    public static List<String> readAllLines(String testRoot) throws IOException {
        return Files.readAllLines(Path.of(testRoot));
    }

    public static List<Path> getAllFileNames(Path root) {
        try (Stream<Path> stream = Files.walk(root)) {

            return stream
                    .sorted().collect(Collectors.toList());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Path qualifiedNameToPath(String root, String fqn) {
        final String[] split = fqn.split("\\.");
        final int last = split.length - 1;
        split[last] = split[last] + ".java";
        return Path.of(root, split);
    }

    public static List<Path> getAllFileNames(Path root, String extension) {
        try (Stream<Path> stream = Files.walk(root)) {

            return stream
//                    .map(String::valueOf)
                    .filter(s -> s.toString().endsWith(extension))
                    .sorted().collect(Collectors.toList());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    public static void replaceInFiles_byLine(List<Path> fileNames, Function<String, String> lineModifier) {
        for (Path path : fileNames) {
            try (Stream<String> lines = Files.lines(path)) {
                List<String> replaced = lines.map(lineModifier).collect(Collectors.toList());
                Files.write(path, replaced);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void replaceInFiles(List<Path> fileNames, Function<String, String> modifier) {
        for (Path path : fileNames) {
            try {
                String whole = Files.readString(path, StandardCharsets.UTF_8);
                String replaced = modifier.apply(whole);
                Files.writeString(path, replaced);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
