package jb.util;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Applies a transformation to all files in a given path
 */

public final class FilesBatchProcessor {
    private static final Path IN_PATH = Paths.get("/home/jbm/workspaces/intellij/v1103-sp__5.9.x");
    private static final List<Path> IN_FILES = FileUtils.getAllFileNames(IN_PATH, ".java");

    public static void main(String[] args) throws IOException {
        final FilesBatchProcessor x = new FilesBatchProcessor();
        x.replaceInFiles();
    }

    private void replaceInFiles() {
        FileUtils.replaceInFiles(IN_FILES, FilesBatchProcessor::replacePattern);
    }

    static private String replacePattern(String fileContent) {
        final String pattern = "SqlHelper.";
        if (!fileContent.contains(pattern))
            return fileContent;

        // A hackish way to add the static import; a special case is for classes in the same package.
        final String imp = "import com.orchestranetworks.cce.rdb.xsmapping.*;";
        final String pack = "package com.orchestranetworks.cce.rdb.xsmapping;";
        final String staticImport = "import static com.orchestranetworks.cce.rdb.xsmapping.SqlHelper.*;";
        return fileContent
//                .replace(pattern, "")
                .replace(imp,
                        imp + "\n" + staticImport)
                .replace(pack,
                        pack + "\n" + staticImport);
    }
}
