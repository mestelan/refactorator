package jb.javaparser;

import com.github.javaparser.ast.visitor.GenericVisitor;
import jb.javaparser.visitor.*;
import jb.util.FileUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Supplier;

/**
 * Registers various ways of modifying the content of Java source files.
 */
final class ClassModifier extends ClassModifier_abstract {

    void searchFields(Path root) throws IOException {
        modifyClassDefinitions(root, StaticImportVisitor::new);
    }

    void searchMarkedMethods(Path root) throws IOException {
        modifyClassDefinitions(root, AnnotatedMethodsVisitor::new);
    }

    void modifyClassesByRemovingCauseOrWholeAnnotation(Path root) throws IOException {
        modifyClassDefinitions(root, AnnotationVisitor::new);
    }

    void modifyMethodByRemovingAnnotation(Path path, String methodName) throws IOException {
        GenericVisitor visitor = new MethodVisitor(methodName);
        modifyClassDefinition(path, visitor);
    }

    void modifyClassByRemovingAnnotation(Path path, String methodName) throws IOException {
        GenericVisitor visitor = new ClassVisitor(methodName);
        modifyClassDefinition(path, visitor);
    }

    private void modifyClassDefinitions(Path root, Supplier<GenericVisitor> supplier) throws IOException {
        List<Path> paths = FileUtils.getAllFileNames(root, ".java");
        for (Path path : paths) {
            System.out.println(path);
            GenericVisitor visitor = supplier.get();
            modifyClassDefinition(path, visitor);
        }
    }
}