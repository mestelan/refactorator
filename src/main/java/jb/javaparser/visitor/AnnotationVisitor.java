package jb.javaparser.visitor;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.visitor.ModifierVisitor;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Given a list of bug references, which are no longer needed.
 * a) We want to remove them from the @IgnoreLimitation annotation.
 * b) We want to remove the @IgnoreLimitation annotation completely if this reference was the only one.
 *
 * Could be refactored with ClassVisitor
 */
public final class AnnotationVisitor extends ModifierVisitor<Void> {

    public static final String IGNORE_LIMITATION = "IgnoreLimitation";
    public static final List<String> REFERENCES_TO_BE_DELETED = Arrays.asList(//
            "LUCENE_CANNOT_USE_SEARCH_TEMPLATES_IN_RELATIONAL_MODE",
            "RMS_M20960_WORKAROUND",
            "B_25778",
            "B_9023",
            "B_26342_HV_RMS",
            "INCOMPATIBLE_WITH_RELATIONAL_MODE",
            "PVR_RELATIONAL_MODE_NOT_SUPPORTED",
            "RMS_ORACLE_SORT_FAILS",
            "AGGREGATED_LIST_NOT_SUPPORTED_FOR_RELATIONAL_MODES",
            "AN_INSTANCE_OF_THIS_SCHEMA_ALREADY_EXISTS_IN_THIS_HOME",
            "INHERITANCE_NOT_SUPPORTED_IN_EXTERNALIZED_MODE",
            "NO_CHILD_FOR_RELATIONAL_DATASPACE",
            "NO_REPLICATION_FOR_RELATIONAL_DATASPACE",
            "SORT_ON_COMPUTED_VALUE_FORBID_ON_RM"
    );

    public AnnotationExpr visit(final NormalAnnotationExpr annotation, final Void arg) {
        super.visit(annotation, arg);
        // Targets our custom annotation: @IgnoreLimitation
        if (!annotation.getNameAsString().equals(IGNORE_LIMITATION)) {
            return annotation;
        }

//      Targets the only member to the annotation  (cause = ...)
        MemberValuePair mvp = annotation.getPairs().get(0);
        if (!mvp.getNameAsString().equals("cause")) {
            throw new IllegalStateException();
        }

        /* The value can be either:
        * - of type FieldAccessExpr, for a single reference; ex: BugReference.INCOMPATIBLE_WITH_RELATIONAL_MODE
        * - of type ArrayInitializerExpr, for multiple references; ex: { BugReference.ARCHIVE42, BugReference.NO_CHILD_FOR_RELATIONAL_DATASPACE }
        *  */

        Expression value = mvp.getValue();
        if ((value instanceof FieldAccessExpr)) {
            String nameAsString = ((FieldAccessExpr) value).getNameAsString();
            if (REFERENCES_TO_BE_DELETED.contains(nameAsString))
                return null;
            return annotation;
        }
        if ((value instanceof ArrayInitializerExpr)) {
            NodeList<Expression> bugReferences = ((ArrayInitializerExpr) value).getValues();

            for (Iterator<Expression> iterator = bugReferences.iterator(); iterator.hasNext(); ) {
                Node bugRef = iterator.next();
                if (!(bugRef instanceof FieldAccessExpr))
                    throw new IllegalStateException();
                {
                    String nameAsString = ((FieldAccessExpr) bugRef).getNameAsString();
                    if (REFERENCES_TO_BE_DELETED.contains(nameAsString)) {
                        bugReferences.remove(bugRef);
                        // Work around https://github.com/javaparser/javaparser/issues/2936
                        break;
                    }
                }
            }
            if (bugReferences.size() == 0)
                return null;
            return annotation;
        }
        throw new IllegalStateException();
    }
}
