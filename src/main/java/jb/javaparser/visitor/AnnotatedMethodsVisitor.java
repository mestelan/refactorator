package jb.javaparser.visitor;

import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.MarkerAnnotationExpr;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.Visitable;

import java.util.Set;

/**
 * This class is a mix of two different use cases.
 * TODO: separate these.
 */
public final class AnnotatedMethodsVisitor extends ModifierVisitor<Void> {

    private static String getFqn(MethodDeclaration method) {
        final ClassOrInterfaceDeclaration classDef = (ClassOrInterfaceDeclaration) method.getParentNode().orElseThrow();
        final String fqn = String.join(".", classDef.getFullyQualifiedName().orElseThrow(), method.getNameAsString());
        return fqn;
    }

    public AnnotatedMethodsVisitor() {
    }

    /**
     * Given a list of test methods reported as too long, escape them by adding an annotation.
     */
    @Override
    public Visitable visit(final MethodDeclaration method, final Void arg) {
        super.visit(method, arg);
        final String fqn;
        try {
            fqn = getFqn(method);
        } catch (Exception e) {
            // Edge cases, not concerned by our scope.
            return method;
        }
        if (LONG_TESTS.contains(fqn)) {
            final NodeList<AnnotationExpr> annotations = method.getAnnotations();
            annotations.add(new MarkerAnnotationExpr(_EBXAnnotations.LONG_RUN_ONLY));
        }
        return method;
    }

    public AnnotationExpr visit(final NormalAnnotationExpr annotation, final Void arg) {
        super.visit(annotation, arg);
        // SEE BELOW: it is not the right type!!!!
        return annotation;
    }

    /**
     * Removes the target annotation at all levels (class/method)
     */
    public Visitable visit(final MarkerAnnotationExpr annotation, final Void arg) {
        super.visit(annotation, arg);

        if (annotation.getNameAsString().equals(_EBXAnnotations.LONG_RUN_ONLY)) {
            return null;
        }
        return annotation;
    }

    static final Set<String> LONG_TESTS = Set.of(
            "com.orchestranetworks.service.archive.TestCofinogaArchive.testLargeAdaptationTreeArchive",
            "com.orchestranetworks.service.archive.TestArchiveCustomImport.testCustomImportArchive",
            "com.orchestranetworks.repository.migration.v5tov6.relationalMode.RepositoryMigrationTest_RMMode_workflowHistory.testMigration",
            "com.orchestranetworks.repository.dspmerge.MultiThreadDataspaceMergeTest.multiThreadedTest",
            "com.orchestranetworks.repository.migration.RepositoryMigrationTest_5914_tpch_sf01.testTpchSf0_1Content",
            "com.orchestranetworks.test.issues.TestIssue21106.testSchemaGeneratingNumerousTableSuffixes",
            "com.orchestranetworks.cce.history.TestHistoryForMergedChildren.testHistoryViewsForMergedChildren_onDspWithManyMergedChildren",
            "com.orchestranetworks.cce.lucene.LuceneIndexRebuildTestLock.test",
            "com.orchestranetworks.service.archive.TestArchiveWithChangeSet.testIssue2405",
            "com.orchestranetworks.cce.lucene.LuceneIndexRebuildTest.test",
            "com.onwbp.adaptation.CfTableRNodeTest.testMergeTableArraySpec",
            "com.onwbp.adaptation.validation.ConcurrentOperationsTest.testLocks",
            "com.orchestranetworks.service.archive.TestArchiveCustomExport.testCustomExport",
            "com.orchestranetworks.cce.cache.dataspace.TestIssue18254.testConcurrency",
            "com.onwbp.adaptation.TestSoftRefClearDuringProcedure.testSoftRefClearDuringProcedure_finishWithoutException",
            "com.orchestranetworks.cce.evolution.TestIssue3061.testAllCombinationsOfSchemaEvolutions",
            "com.orchestranetworks.cce.cache.dataspace.TestIssue10489.testConcurrency",
            "com.orchestranetworks.cce.evolution.TestIssue3061_singleTable.testSchemaEvolutions",
            "com.onwbp.adaptation.test.request.TestInheritanceCombinations.testNominalCases",
            "com.orchestranetworks.repository.compilation.TestIssue3952_A.testMultipleThreadsAccessingOneTableInOneUnloadedBranchWithRefresh",
            "com.orchestranetworks.cce.evolution.TestIssue3061_simple.testSimpleSchemaEvolutions",
            "com.orchestranetworks.repository.migration.RepositoryMigrationTest_5914_revisionSharing.testMigration",
            "com.orchestranetworks.eventbroker.SubscriberTestForHome.testHomeSubscriber",
            "com.orchestranetworks.cce.tablePagedView.TablePagedViewTest_timeout.testTablePagedViewTimeout",
            "com.orchestranetworks.repository.migration.RepositoryMigrationTest_from550_issue289.testRepositoryInitialization",
            "com.onwbp.adaptation.RequestPaginationTest_timeout.testRequestPaginationTimeout",
            "com.onwbp.adaptation.validation.persistent.PvrTest_xpathValidationRule.test2",
            "com.orchestranetworks.cce.rdb.rro.RroOwnershipTest.testFailoverMain_vs_FailoverStandby_activateThenAccess",
            "com.orchestranetworks.cce.history.TestHistoryForMergedChildren.testHistoryViewsForMergedChildren",
            "com.orchestranetworks.repository.compilation.TestIssue9462.testMultipleThreadsAccessingOneTableInOneUnloadedBranch",
            "com.orchestranetworks.repository.compilation.TestIssue9462.testMultipleThreadsAccessingMultipleTablesInOneUnloadedBranch",
            "com.orchestranetworks.repository.compilation.TestIssue9462.testMultipleThreadsAccessingMultipleTablesInTwoUnloadedBranches",
            "com.orchestranetworks.workflow.test.NewWorkflowUnitTest.testNewProductProcessConditionFalse",
            "com.orchestranetworks.utils.bench.TestIssue11466.testParallelUpgThreadsOnDifferentHomes",
            "com.onwbp.adaptation.validation.TestDuplicateAndChildDatasetValidation_CP20381.randomizedTest",
            "com.onwbp.adaptation.validation.DeadLockInterInstanceNotificationsTest.deadLockInterInstanceNotificationsTest",
            "com.orchestranetworks.service.archive.TestArchiveWithChangeSet.testImportArchive",
            "com.orchestranetworks.cce.cache.store.rdb.RepositoryMigrationTest_5913_ExcludingFailingSchema_tutorial.testMigrationExcludingFailingSchema",
            "com.onwbp.adaptation.validation.ConstraintOnTable_DependenciesToTableTest.testModification",
            "com.orchestranetworks.service.archive.XMLImportTest_Issue3713.testAPI",
            "com.orchestranetworks.cce.calcite.TestQueryExecutionFrameworkWithTimeout.testGetSizeOrTimeout",
            "com.orchestranetworks.repository.migration.RepositoryMigrationTest_5911_withErrorDuringSchemaCompilation.testRepositoryInitialization",
            "com.orchestranetworks.cce.icc.IccSimpleXsDateTest.testRandomDates",
            "com.orchestranetworks.cce.icc.IccSimpleXsDate_SearchLimitations_Test.testRandomDates",
            "com.orchestranetworks.cce.history.TestHistoryForAllDataspaces.testHistoryViews_onDspWithManyMergedChildren",
            "com.orchestranetworks.repository.concurrency.MultiThreadedReadAndWriteAccessTest.monoThreadedTest",
            "com.onwbp.adaptation.validation.TestIssue5138.testConcurrencyOnInterInstanceLinks",
            "com.onwbp.adaptation.validation.INAValidationTest.testDeletion",
            "com.onwbp.adaptation.validation.XsUniqueValidationParentPathTest.testModification",
            "com.onwbp.adaptation.validation.XsUniqueValidationSelfPathTest.testModification",
            "com.onwbp.adaptation.validation.XsUniqueValidationSimpleElementTest.testModification",
            "com.orchestranetworks.repository.migration.RepositoryMigrationTest_schemaLocalFileMigration_from514.testMigration",
            "com.onwbp.adaptation.validation.persistent.PvrTest_fieldInheritanceDependency.inheritanceOneTableLink_ThreeTimes",
            "com.orchestranetworks.repository.migration.RepositoryMigrationTest_from527.testValidAccess",
            "com.onwbp.adaptation.validation.INAValidationTest.testModification",
            "com.orchestranetworks.repository.migration.RepositoryMigrationTest_from526fix002.testValidAccess",
            "com.orchestranetworks.repository.migration.RepositoryMigrationTest_schemaLocalFileMigration_from514.testSchemaLocalFileCompilation",
            "com.orchestranetworks.repository.migration.RepositoryMigrationTest_from515_bug5855.testValidAccess",
            "com.onwbp.adaptation.validation.OSDSelectMinOccursErrorMessagesTest.testModification",
            "com.onwbp.adaptation.validation.XsUniqueValidationComplexPathTest.testModification",
            "com.onwbp.adaptation.validation.XsUniqueValidationInRootTest.testModification",
            "com.onwbp.adaptation.validation.OSDSelectMinOccursErrorMessagesTest.testDeletion",
            "com.onwbp.com.google.protobuf.DescriptorFormatParseTest.shouldRoundTripFormattingOnPermissionsTable",
            "com.onwbp.adaptation.validation.ConstraintOnTable_case2WithUnknownDependenciesTest.testModification",
            "com.onwbp.adaptation.validation.persistent.PvrTest_associationDependency.associationByXPathComplex",
            "com.orchestranetworks.cce.icc.IccSimpleXsDateTimeTest.testRandomDates",
            "com.onwbp.adaptation.validation.OSDSelectValidationTest.testModification",
            "com.onwbp.adaptation.validation.OSDSelect_container_ConstraintValidationTest.testModification",
            "com.onwbp.adaptation.validation.OSDSelectValidationTest.testDeletion",
            "com.onwbp.adaptation.validation.UnknownConstraintWithDependenciesInTableTest.testModification");
}
