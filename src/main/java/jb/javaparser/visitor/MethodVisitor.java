package jb.javaparser.visitor;

import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.Visitable;

import java.util.Iterator;

/**
 * Given a test method currently escaped (by one or several bug references, it does not matter):
 * This test method is known to actually pass, so we want to remove the @IgnoreLimitation annotation from it.
 */
public final class MethodVisitor extends ModifierVisitor<Void> {
    private final String targetMethodName;

    public MethodVisitor(String methodName) {
        this.targetMethodName = methodName;
    }

    @Override
    public Visitable visit(final MethodDeclaration n, final Void arg) {
        super.visit(n, arg);
        final String nameAsString = n.getNameAsString();
        if (!nameAsString.equals(targetMethodName)) {
            return n;
        }

        final NodeList<AnnotationExpr> annotations = n.getAnnotations();
        removeIgnoreLimitation(annotations);
        return n;
    }


    private void removeIgnoreLimitation(NodeList<AnnotationExpr> annotations) {
        for (Iterator<AnnotationExpr> ite = annotations.iterator(); ite.hasNext(); ) {
            AnnotationExpr annotation = ite.next();
            if (annotation.getNameAsString().equals(_EBXAnnotations.IGNORE_LIMITATION)) {
                ite.remove();
            }
        }
    }
}
