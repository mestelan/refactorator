package jb.javaparser.visitor;

/**
 * Some constant names.
 */
public final class _EBXAnnotations {
    public static final String IGNORE_LIMITATION = "IgnoreLimitation";
    public static final String LONG_RUN_ONLY = "LongRunOnly";
}
