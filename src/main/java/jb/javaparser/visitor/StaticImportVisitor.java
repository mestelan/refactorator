package jb.javaparser.visitor;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.FieldAccessExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.expr.VariableDeclarationExpr;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.Visitable;

import java.util.HashSet;
import java.util.Set;

/**
 * Searches for occurrences of 'SqlHelper.xxxx' and replaces them with a static import.
 */
public final class StaticImportVisitor extends ModifierVisitor<Void> {

    private static final String SQL_HELPER = "SqlHelper";
    private boolean foundFieldRequiringStaticImport;
    private boolean isFirstPass = true;
    private final Set<String> names = new HashSet<>();

    /**
     * We make two passes, to avoid removing the scope when this would lead to ambiguity.
     */
    @Override
    public Visitable visit(final CompilationUnit n, final Void arg) {
        super.visit(n, arg);
        this.isFirstPass = false;
        super.visit(n, arg);
        if (foundFieldRequiringStaticImport) {
            n.addImport(new ImportDeclaration("com.orchestranetworks.cce.rdb.xsmapping.SqlHelper", true, true));
        }
        return n;
    }

    /**
     * A {@link NameExpr } is of the form: x
     * We collect these on the first pass, to avoid introducing ambiguous names.
     */
    @Override
    public Visitable visit(final FieldDeclaration n, final Void arg) {
        super.visit(n, arg);
        if (isFirstPass) {
            for (VariableDeclarator variable : n.getVariables()) {
                final String nameAsString = variable.getNameAsString();
                names.add(nameAsString);
            }
        }
        return n;
    }

    /**
     * A {@link FieldAccessExpr} is of the form: person.name
     */
    @Override
    public Visitable visit(final FieldAccessExpr n, final Void arg) {
        super.visit(n, arg);
        if (isFirstPass) {
            return n;
        }

        final Expression scopeExpr = n.getScope();
        if (!(scopeExpr instanceof NameExpr)) {
            return n;
        }

        if (SQL_HELPER.equals(((NameExpr) scopeExpr).getNameAsString())) {
            final String name = n.getNameAsString();
            if (names.contains(name)) {
                System.err.println("caution here");
                return n;
            }
            this.foundFieldRequiringStaticImport = true;
            return new NameExpr(name);
        }

        return n;
    }
}
