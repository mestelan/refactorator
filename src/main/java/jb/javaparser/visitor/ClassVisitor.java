package jb.javaparser.visitor;

import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import com.github.javaparser.ast.visitor.ModifierVisitor;

/**
 * Given a method currently escaped (by one or several bug references, it does not matter):
 * The test actually passes, so we want to remove the @IgnoreLimitation annotation.
 */
public final class ClassVisitor extends ModifierVisitor<Void> {
    private final String methodName;

    public ClassVisitor(String methodName) {
        this.methodName = methodName;
    }

    /**
     * Removes the target annotation at all levels (class/method)
     */
    public AnnotationExpr visit(final NormalAnnotationExpr annotation, final Void arg) {
        super.visit(annotation, arg);
        // Targets our custom annotation: @IgnoreLimitation
        if (annotation.getNameAsString().equals(AnnotationVisitor.IGNORE_LIMITATION)) {
            return null;

        }
        return annotation;
    }
}
