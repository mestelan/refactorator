package jb.javaparser;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.visitor.GenericVisitor;
import com.github.javaparser.printer.PrettyPrinter;
import com.github.javaparser.printer.PrettyPrinterConfiguration;
import com.github.javaparser.printer.lexicalpreservation.LexicalPreservingPrinter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

abstract class ClassModifier_abstract {
    /**
     */
    protected void modifyClassDefinition(Path path, GenericVisitor visitor) throws IOException {
        CompilationUnit compilationUnit = readClassDefinitionWithLexicalPreservingPrinter(path);
        visitor.visit(compilationUnit, null);
        writeToFile(true, path, compilationUnit);
    }

    private CompilationUnit readClassDefinitionWithLexicalPreservingPrinter(Path fileName) throws IOException {
        JavaParser parser = new JavaParser();
        parser.getParserConfiguration().setLexicalPreservationEnabled(true);
        return parser.parse(fileName).getResult().get();

    }

    private void writeToFile(boolean lexical, Path filePath, CompilationUnit compilationUnit) {
        String print;
        if (lexical) {
            print = LexicalPreservingPrinter.print(compilationUnit);
        } else {
            PrettyPrinter printer = configurePrinter();
            print = printer.print(compilationUnit);
        }

        try (FileOutputStream fos = new FileOutputStream(filePath.toFile());
             Writer writer = new OutputStreamWriter(fos, StandardCharsets.UTF_8)) {
            writer.write(print);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private PrettyPrinter configurePrinter() {
        PrettyPrinterConfiguration conf = new PrettyPrinterConfiguration();
        conf.setIndentType(PrettyPrinterConfiguration.IndentType.TABS);
        return new PrettyPrinter(conf);
    }
}
